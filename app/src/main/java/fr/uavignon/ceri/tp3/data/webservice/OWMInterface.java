package fr.uavignon.ceri.tp3.data.webservice;

import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface OWMInterface {

    @Headers("Accept: application/geo+json")
    @GET("/City")
    Call<WeatherRoomDatabase> getForecast(@Path("office") String office,
                                          @Path("gridX") int gridX,
                                          @Path("gridY") int gridY);




}
